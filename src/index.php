<?php

// Read env variables
$mysql_server = $_ENV["MYSQL_SERVER"];
$mysql_database = $_ENV["MYSQL_DATABASE"];
$mysql_user = $_ENV["MYSQL_USER"];
$mysql_password = $_ENV["MYSQL_PASSWORD"];

// Create connection
$conn = mysqli_connect($mysql_server, $mysql_user, $mysql_password);

// Check connection
if (!$conn) {
  die("<div style='background-color: #ffc0cb'>Connection failed: " . mysqli_connect_error() . "</div>");
}
echo "<div style='background-color: #cfc'>";
echo "Connected successfully";
echo "</div>";

?>
