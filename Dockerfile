FROM php:8.0.9-fpm-alpine

RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

RUN apk --update add redis
