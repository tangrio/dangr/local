# Tangrio Demo app on PHP

## Components

- PHP-FPM
- nginx
- Dockerfile locally built
- mysql database
- docker-compose
- redis
- elasticsearch
- tideways daemon
- more to come ...

## Use

Browse http://localhost:8080/
